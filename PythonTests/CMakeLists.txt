
# Include Testing package
if(BUILD_TESTING)

  # ========================================================================
  # ========================================================================
  # Tests
  # ========================================================================
  # ========================================================================

  include( ${CMAKE_SOURCE_DIR}/OpenFlipper/tests/testGenerators.cmake )

  # ========================================================================
  # ========================================================================
  # Tests
  # ========================================================================
  # ========================================================================
  run_algorithm_test( "smoother_c0_normal_10.ofp" "cube_sharp.off" "result_cube_sharp_smooth_normal_c0_10.off"  )



endif()
